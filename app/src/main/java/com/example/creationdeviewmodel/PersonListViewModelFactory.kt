package com.example.creationdeviewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

// pour solution 5.1 et 5.2 uniquement
//class PersonListViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
//    @Suppress("UNCHECKED_CAST")
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//
//        if (modelClass.isAssignableFrom(PersonListViewModel::class.java)) {
//            return PersonListViewModel(repository) as T
//        }
//
//        throw IllegalArgumentException("Unknown ViewModel class")
//    }
//}
