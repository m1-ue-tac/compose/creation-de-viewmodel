package com.example.creationdeviewmodel

import android.util.Log

// pour solution 5.1
// Si vous tournez le téléphone, vous lancez onCreate de l'activité, et donc vous recréez un
// Repository. Vous pourrez le voir dans le log, le n° de l'objet change à chaque fois.
//  class Repository {
//    init {
//        Log.d("JC_ViewModel", "création du Repository $this")
//    }
//  }

// pour solution 5.2 et 5.3
// Le Repository est maintenant un Singleton 'à la Kotlin', c-à-d un object et non plus une classe.
//object Repository {
//    init {
//        Log.d("ViewModel", "création du Repository $this")
//    }
//
//    fun initialise(uneChaine: String): Repository {
//        Log.d("ViewModel", "je suis LE Repository $this, et je suis initialisé avec " + uneChaine)
//        return this
//    }
//}
