package com.example.creationdeviewmodel

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModelProvider
import com.example.creationdeviewmodel.ui.theme.CreationDeViewModelTheme

class MainActivity : ComponentActivity() {

    // Solution 2
    // lateinit var ==> on initialise ainsi la variable lors de son premier usage, et pas à sa déclaration.
    //      private lateinit var viewModel : PersonListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        // ----------- sans passage de paramètre -----------
        // Solution 1.
        // On crée le viewmodel et on l'initialise de suite
        val viewModel = ViewModelProvider(this).get(PersonListViewModel::class.java)

        // Solution 2.
        // on initialise ICI la variable (premier usage), et pas à sa déclaration.
        //          viewModel = ViewModelProvider(this).get(PersonListViewModel::class.java)

        // Solution 3.
        // Avec val et by lazy --> création du ViewModel uniquement si nécessaire. Cela peut être
        // utile pour retarder la création d'objets coûteux jusqu'à ce qu'ils soient réellement nécessaires
        //      val viewModel: PersonListViewModel by lazy {
        //                ViewModelProvider(this).get(PersonListViewModel::class.java)
        //            }

        // Solution 4.
        // même principe que la solution 3 précédente. Le code fait la même chose que 'by lazy' sans avoir
        // besoin de spécifier le ViewModelProvider. Il le détermine automatiquement pour vous.
        //      val viewModel: PersonListViewModel by viewModels()

        // --------- avec passage de paramètre ----------

        // Solutions 5

        // Solution 5.1.
        // Le souci est qu'on crée toujours des nouvelles instances de Repository, à chaque rotation d'écran...
        //      val viewModel: PersonListViewModel by viewModels {
        //          PersonListViewModelFactory(Repository())
        //       }

        // Solution 5.2.
        // Idem que 5.1, mais Repository en Singleton, même si rotation d'écran...
        //      val viewModel: PersonListViewModel by viewModels {
        //          PersonListViewModelFactory(Repository)
        //      }

        // Solution 5.3.
        // Code + actuel pour les Factory (en classe interne) et Repository en Singleton.
        // Ici, le ViewModel instancie le Repository et l'initialise ==> couplage fort ! -> solution : injection de dépendances
        //      val viewModel: PersonListViewModel by viewModels {
        //          PersonListViewModel.Factory(Repository.initialise("ViewModel"))
        //      }

        Log.d("JC_ViewModel", "je suis le ViewModel $viewModel")

        setContent {
            CreationDeViewModelTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PersonListApp(viewModel)
                }
            }
        }
    }
}

@Composable
fun PersonListApp(viewModel: PersonListViewModel) {
    // on récupère les données depuis le viewmodel. C'est un State donc recomposition automatique
    // en cas de changement de valeurs
    val persons = viewModel.persons

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Text(text = "Person List", style = TextStyle(fontSize = 24.sp))
        Spacer(modifier = Modifier.height(16.dp))

        // Ajout d'une nouvelle personne
        var newPersonName by remember { mutableStateOf("") }
        TextField(
            value = newPersonName,
            onValueChange = { newPersonName = it },
            label = { Text(text = "New Person Name") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        )
        Button(
            enabled = newPersonName.isNotBlank(),
            onClick = {
                if (newPersonName.isNotBlank()) {
                    viewModel.addPerson(newPersonName)
                    newPersonName = ""
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text(text = "Add Person")
        }

        Spacer(modifier = Modifier.height(16.dp))

        // Affichage de la liste des personnes dans des cards
        LazyColumn {
            items(persons.value) { person ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(text = person.name, style = TextStyle(fontSize = 20.sp))
                        Spacer(modifier = Modifier.width(16.dp))
                        Button(
                            onClick = { viewModel.removePerson(person) },
                            content = {
                                Text(text = "Remove")
                            }
                        )
                    }
                }
            }
        }
    }
}
