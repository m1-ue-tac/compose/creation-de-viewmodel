package com.example.creationdeviewmodel

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel

// ViewModel

// sans passage de paramètre (solution 1 à 4)
class PersonListViewModel() : ViewModel() {

// avec passage de paramètre (solutions 5). Dans ce cas, en théorie le Repository sera
// utilisé pour fournir les données par exemple. Ainsi dans init, on aurait plutôt :
//       _persons.value = repository.getPersons()
//class PersonListViewModel(private val repository: Repository) : ViewModel() {


    // Utilisation de MutableState pour gérer la liste des personnes
    private var _persons = mutableStateOf<List<Person>>(listOf())

    // Exposer la liste en tant que State pour la lecture seule
    val persons: State<List<Person>> = _persons

    init {
        Log.d("JC_ViewModel", "création du viewmodel $this")

        // ici on construit en dur la liste. En général, ces valeurs proviennent de couches
        // inférieures telles que Use Case, Repository,...
        _persons.value = mutableListOf(
                Person(1, "Alice"),
                Person(2, "Bob"),
                Person(3, "Charlie")
        )
    }

    fun addPerson(name: String) {
        val newId = (_persons.value.maxByOrNull { it.id }?.id ?: 0) + 1
        val newPerson = Person(newId, name+"JC")

        // Mise à jour de la liste par réaffectation car on ne peut pas faire simplement .add
        //  autre solution
        //   val currentPersons = _persons.value.toMutableList() // Créez une copie mutable de la liste actuelle
        //   val newPerson = Person(newId, "Nouveau Nom") // Créez une nouvelle personne avec les données appropriées
        //   currentPersons.add(newPerson) // Ajoutez la nouvelle personne à la liste
        //   _persons.value = currentPersons // Réaffectez la liste mise à jour à _persons
        _persons.value += listOf(newPerson)
    }

    fun removePerson(person: Person) {
        // Mise à jour de la liste par réaffectation car on ne peut pas faire simplement .remove
        //        _persons.value.remove(person)
        _persons.value = _persons.value.filter { it != person }
    }

    // pour solution 5.3 uniquement

    // Intégration de la Factory dans la classe associée (recommandé par Google,
    // cf. https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories?hl=fr#creationextras)
//
//    class Factory(private val repository: Repository) : ViewModelProvider.Factory {
//        @Suppress("UNCHECKED_CAST")
//        override fun <T : ViewModel> create(modelClass: Class<T>): T {
//
//            if (modelClass.isAssignableFrom(PersonListViewModel::class.java)) {
//                return PersonListViewModel(repository) as T
//            }
//
//            throw IllegalArgumentException("Unknown ViewModel class")
//        }
//
//    }

}